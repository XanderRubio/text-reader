import io
import os
from google.cloud import vision
from google.cloud.vision import types
from dotenv import load_dotenv

# Load the API key from the .env file
load_dotenv()
api_key = os.getenv('GOOGLE_CLOUD_API_KEY')

# Set up the Google Cloud Vision API client with the API key
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = api_key
client = vision.ImageAnnotatorClient()

# Load the image file
with io.open('path/to/your/image.jpg', 'rb') as image_file:
    content = image_file.read()

# Convert the image file to a Google Cloud Vision API image object
image = types.Image(content=content)

# Send the image to the Google Cloud Vision API for text detection
response = client.text_detection(image=image)
texts = response.text_annotations

# Print the detected text
for text in texts:
    print('\n"{}"'.format(text.description))
    
